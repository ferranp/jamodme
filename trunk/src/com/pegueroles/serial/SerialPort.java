package com.pegueroles.serial;

public class SerialPort {
    public static final int FLOWCONTROL_NONE = 1;
    public static final int DATABITS_5=2;
    public static final int DATABITS_6=3;
    public static final int DATABITS_7=4;
    public static final int DATABITS_8=5;
    public static final int STOPBITS_1=6;
    public static final int STOPBITS_1_5=61;
    public static final int STOPBITS_2=62;
    public static final int PARITY_NONE=7;
    public static final int PARITY_EVEN=71;
    public static final int PARITY_ODD=72;
    public static final int FLOWCONTROL_XONXOFF_OUT=8;
    public static final int FLOWCONTROL_XONXOFF_IN=9;
    public static final int FLOWCONTROL_RTSCTS_IN=11;
    public static final int FLOWCONTROL_RTSCTS_OUT=12;

}
